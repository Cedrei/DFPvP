Class = {
	init: function () {
		$("#skill1").css("background-image", 'url("img/icon/poison.png")')
		$("#skill2").css("background-image", 'url("img/icon/chicken.png")')
		$("#skill3").css("background-image", 'url("img/icon/manastrike.png")')
		$("#skill4").css("background-image", 'url("img/icon/fear.png")')
		$("#skill5").css("background-image", 'url("img/icon/smokedeye.png")')
		$("#skill6").css("background-image", 'url("img/icon/meditate.png")')
		$("#skill7").css("background-image", 'url("img/icon/shield.png")')
		$("#skill8").css("background-image", 'url("img/icon/power.png")')
		$("#skill9").css("background-image", 'url("img/icon/fire.png")')
		$("#skill10").css("background-image", 'url("img/icon/ice.png")')
		$("#skill11").css("background-image", 'url("img/icon/wind.png")')
		$("#skill12").css("background-image", 'url("img/icon/lightning.png")')
		$("#skill13").css("background-image", 'url("img/icon/multi.png")')
		$("#skill14").css("background-image", 'url("img/icon/skull.png")')
	},
	skillHP: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/HPpot.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "45px")

		$("#"+player+">.projectile").animate({
			left: "-=15",
			top: "-=45"
		}, {
			easing: "linear",
			duration: 600,
			step: function(now, tween) {
				$("#"+player+">.projectile").css({
					transform: "rotate("+(360-tween.pos*90)+"deg)"
				})
			},
			done: function() {
				setTimeout(function() {
					$("#"+player+">.projectile").attr("src", "")
					$("#"+player+">.projectile").css("left", "45px")
					$("#"+player+">.projectile").css("top", "45px")
				}, 300)
			}
		})
	},
	skillMP: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/MPpot.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "45px")

		$("#"+player+">.projectile").animate({
			left: "-=15",
			top: "-=45"
		}, {
			easing: "linear",
			duration: 600,
			step: function(now, tween) {
				$("#"+player+">.projectile").css({
					transform: "rotate("+(360-tween.pos*90)+"deg)"
				})
			},
			done: function() {
				setTimeout(function() {
					$("#"+player+">.projectile").attr("src", "")
					$("#"+player+">.projectile").css("left", "45px")
					$("#"+player+">.projectile").css("top", "45px")
				}, 300)
			}
		})
	},
	skill1: function(player) {
		$("#"+player+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill2: function(player) {
		$("#"+player+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(360-tween.pos*90)+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(tween.pos*90+270)+"deg)"
				})
			}
		})
	},
	skill3: function(player) {
		$("#"+player+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill4: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/smoke.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "45px")

		$("#"+player+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			done: function() {
				$("#"+player+">.projectile").attr("src", "")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")
			}
		})

	},
	skill5: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/smoke.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "45px")

		$("#"+player+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			done: function() {
				$("#"+player+">.projectile").attr("src", "")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")
			}
		})
	},
	skill6: function(player) {
		$("#"+player+"").animate({
			top: "-=50",
		}, {
			duration: 500,
			easing: "easeInOutQuad"
		}).delay(800).animate({
			top: "+=50",
		}, {
			duration: 500,
			easing: "easeInOutQuad"
		})
	},
	skill7: function(player) {
		$("#"+player+"-static").attr("src", "img/shield.png")
		$("#"+player+"-static").css("height", "320px")
		$("#"+player+"-static").css("left", "-80px")
		$("#"+player+"-static").css("top", "20px")

		$("#"+player+"").animate({
			top: "-=2"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*180+"deg)"
				})
				$("#"+player+">.projectile").css({
					transform: "rotate("+(360-tween.pos*180)+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			top: "+=2"
		}, {
			easing: "easeOutQuad",
			duration: 400,
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(tween.pos*180+180)+"deg)"
				})
				$("#"+player+">.projectile").css({
					transform: "rotate("+(180-tween.pos*180)+"deg)"
				})
			}
		})
	},
	skill0: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/lightningball.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "45px")

		$("#"+player+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			done: function() {
				$("#"+player+">.projectile").attr("src", "")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")
			}
		})
	},
	skill8: function(player) {
		$("#"+player+"").animate({
			top: "-=50",
		}, {
			duration: 500,
			easing: "easeInOutQuad"
		}).delay(800).animate({
			top: "+=50",
		}, {
			duration: 500,
			easing: "easeInOutQuad",
			done: function() {
				$("#"+player+">.projectile").attr("src", "img/icon/lightningball.png")
				$("#"+player+">.projectile").css("height", "45px")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")

				$("#"+player+">.projectile").animate({
					left: "+=430"
				}, {
					easing: "linear",
					duration: 1000,
					done: function() {
						$("#"+player+">.projectile").attr("src", "")
						$("#"+player+">.projectile").css("left", "45px")
						$("#"+player+">.projectile").css("top", "45px")
					}
				})
			}
		})
	},
	skill9: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/fire.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "45px")
		$("#"+player+">.projectile").css("transform", "rotate(270deg)")

		$("#"+player+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			done: function() {
				$("#"+player+">.projectile").attr("src", "")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")
			}
		})
	},
	skill10: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/snowflake.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "45px")
		$("#"+player+">.projectile").css("transform", "rotate(90deg)")

		$("#"+player+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			done: function() {
				$("#"+player+">.projectile").attr("src", "")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")
			}
		})
	},
	skill11: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/wind.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "45px")
		$("#"+player+">.projectile").css("transform", "rotate(0deg)")


		$("#"+player+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			done: function() {
				$("#"+player+">.projectile").attr("src", "")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")
			}
		})
	},
	skill12: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/lightningball.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "45px")

		$("#"+player+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			done: function() {
				$("#"+player+">.projectile").attr("src", "")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")
			}
		})
	},
	skill13: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/lightningball.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "45px")

		$("#"+player+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			done: function() {
				$("#"+player+">.projectile").attr("src", "")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")
			}
		})
	},
	skill14: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/lightningball.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "45px")

		$("#"+player+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			done: function() {
				$("#"+player+">.projectile").attr("src", "")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")
			}
		})
	}
}