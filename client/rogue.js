Class = {
	init: function () {
		$("#skill1").css("background-image", 'url("img/icon/HPpot.png")')
		$("#skill2").css("background-image", 'url("img/icon/fear.png")')
		$("#skill3").css("background-image", 'url("img/icon/chicken.png")')
		$("#skill4").css("background-image", 'url("img/icon/knife.png")')
		$("#skill5").css("background-image", 'url("img/icon/stun.png")')
		$("#skill6").css("background-image", 'url("img/icon/smokegrey.png")')
		$("#skill7").css("background-image", 'url("img/icon/double.png")')
		$("#skill8").css("background-image", 'url("img/icon/smokedeye.png")')
		$("#skill9").css("background-image", 'url("img/icon/triple.png")')
		$("#skill10").css("background-image", 'url("img/icon/poison.png")')
		$("#skill11").css("background-image", 'url("img/icon/manastrike.png")')
		$("#skill12").css("background-image", 'url("img/icon/multi.png")')
		$("#skill13").css("background-image", 'url("img/icon/fear.png")')
		$("#skill14").css("background-image", 'url("img/icon/skull.png")')
	},
	skillHP: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/HPpot.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "85px")
		$("#"+player+">.projectile").css("top", "120px")
		$("#"+player+">.projectile").css("transform", "rotate(60deg)")

		$("#"+player+">.projectile").animate({
			left: "+=45",
			top: "-=25"
		}, {
			easing: "linear",
			duration: 400,
			step: function(now, tween) {
				$("#"+player+">.projectile").css({
					transform: "rotate("+(60-tween.pos*60)+"deg)"
				})
			}
		})

		$("#"+player+">.projectile").animate({
			left: "-=10",
			top: "-=40"
		}, {
			easing: "linear",
			duration: 400,
			step: function(now, tween) {
				$("#"+player+">.projectile").css({
					transform: "rotate("+(360-tween.pos*80)+"deg)"
				})
			},
			done: function() {
				setTimeout(function() {
					$("#"+player+">.projectile").attr("src", "")
					$("#"+player+">.projectile").css("left", "45px")
					$("#"+player+">.projectile").css("top", "45px")
				}, 300)
			}
		})
	},
	skillMP: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/MPpot.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "85px")
		$("#"+player+">.projectile").css("top", "120px")
		$("#"+player+">.projectile").css("transform", "rotate(60deg)")

		$("#"+player+">.projectile").animate({
			left: "+=45",
			top: "-=25"
		}, {
			easing: "linear",
			duration: 400,
			step: function(now, tween) {
				$("#"+player+">.projectile").css({
					transform: "rotate("+(60-tween.pos*60)+"deg)"
				})
			}
		})

		$("#"+player+">.projectile").animate({
			left: "-=10",
			top: "-=40"
		}, {
			easing: "linear",
			duration: 400,
			step: function(now, tween) {
				$("#"+player+">.projectile").css({
					transform: "rotate("+(360-tween.pos*80)+"deg)"
				})
			},
			done: function() {
				setTimeout(function() {
					$("#"+player+">.projectile").attr("src", "")
					$("#"+player+">.projectile").css("left", "45px")
					$("#"+player+">.projectile").css("top", "45px")
				}, 300)
			}
		})
	},
	skill1: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/HPpot.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "85px")
		$("#"+player+">.projectile").css("top", "120px")
		$("#"+player+">.projectile").css("transform", "rotate(60deg)")

		$("#"+player+">.projectile").animate({
			left: "+=45",
			top: "-=25"
		}, {
			easing: "linear",
			duration: 400,
			step: function(now, tween) {
				$("#"+player+">.projectile").css({
					transform: "rotate("+(60-tween.pos*60)+"deg)"
				})
			}
		})

		$("#"+player+">.projectile").animate({
			left: "-=10",
			top: "-=40"
		}, {
			easing: "linear",
			duration: 400,
			step: function(now, tween) {
				$("#"+player+">.projectile").css({
					transform: "rotate("+(360-tween.pos*80)+"deg)"
				})
			},
			done: function() {
				setTimeout(function() {
					$("#"+player+">.projectile").attr("src", "")
					$("#"+player+">.projectile").css("left", "45px")
					$("#"+player+">.projectile").css("top", "45px")
				}, 300)
			}
		})
	},
	skill2: function(player) {
		$("#"+player+"").animate({
			left: "+=440",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=440",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill3: function(player) {
		$("#"+player+"").animate({
			left: "+=400",
			top: "-=65"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(360-tween.pos*90)+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=400",
			top: "+=65"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(tween.pos*90+270)+"deg)"
				})
			}
		})
	},
	skill4: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/knife.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "75px")

		$("#"+player+">.projectile").animate({
			left: "+=450"
		}, {
			easing: "linear",
			duration: 1000,
			step: function(now, tween) {
				$("#"+player+">.projectile").css({
					transform: "rotate("+(tween.pos*1000%360)+"deg)"
				})
			},
			done: function() {
				$("#"+player+">.projectile").attr("src", "")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")
			}
		})
	},
	skill5: function(player) {
		$("#"+player+"").animate({
			left: "+=480",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*150+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			top: "+=95"
		}, {
			easing: "linear",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(150-tween.pos*150)+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=480"
		}, {
			easing: "easeOutQuad",
			duration: 800
		})
	},
	skill6: function(player) {
		$("#"+player+"-static").attr("src", "img/smoke.png")
		$("#"+player+"-static").css("height", "320px")
		$("#"+player+"-static").css("left", "-20px")
		$("#"+player+"-static").css("top", "30px")

		setTimeout(function(){
			$("#"+player+"-static").animate({
				opacity: 0
			}, {
				duration: 1000,
				done: function() {
					$("#"+player+"-static").attr("src", "")
					$("#"+player+"-static").css("opacity", 1)
				}
			})
			
		}, 1000)
	},
	skill7: function(player) {
		$("#"+player+"").animate({
			left: "+=480",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*150+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			top: "+=95"
		}, {
			easing: "linear",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(150-tween.pos*150)+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=280"
		}, {
			easing: "easeOutQuad",
			duration: 300
		})
		$("#"+player+"").animate({
			left: "+=280",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*150+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			top: "+=95"
		}, {
			easing: "linear",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(150-tween.pos*150)+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=480"
		}, {
			easing: "easeOutQuad",
			duration: 300
		})
	},
	skill0: function(player) {
		$("#"+player+"").animate({
			left: "+=480",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*150+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			top: "+=95"
		}, {
			easing: "linear",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(150-tween.pos*150)+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=480"
		}, {
			easing: "easeOutQuad",
			duration: 800
		})
	},
	skill8: function(player) {
		$("#"+player+"").animate({
			opacity: 0
		}, {
			duration: 1000,
			easing: "easeInOutQuad"
		})
		$("#"+player+"").animate({
			opacity: 1
		}, {
			duration: 500,
			easing: "easeInOutQuad"
		})
	},
	skill9: function(player) {
		$("#"+player+"").animate({
			left: "+=440",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=440",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill10: function(player) {
		$("#"+player+"").animate({
			left: "+=440",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=440",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill11: function(player) {
		$("#"+player+"").animate({
			left: "+=440",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=440",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill12: function(player) {
		$("#"+player+">.projectile").attr("src", "img/icon/knife.png")
		$("#"+player+">.projectile").css("height", "45px")
		$("#"+player+">.projectile").css("left", "45px")
		$("#"+player+">.projectile").css("top", "75px")

		$("#"+player+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			step: function(now, tween) {
				$("#"+player+">.projectile").css({
					transform: "rotate("+(tween.pos*1000%360)+"deg)"
				})
			},
			done: function() {
				$("#"+player+">.projectile").attr("src", "")
				$("#"+player+">.projectile").css("left", "45px")
				$("#"+player+">.projectile").css("top", "45px")
			}
		})
	},
	skill13: function(player) {
		$("#"+player+"").animate({
			left: "+=440",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=440",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill14: function(player) {
		$("#"+player+"").animate({
			left: "+=480",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*150+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			top: "+=95"
		}, {
			easing: "linear",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(150-tween.pos*150)+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=280"
		}, {
			easing: "easeOutQuad",
			duration: 300
		})
		$("#"+player+"").animate({
			left: "+=280",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+tween.pos*150+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			top: "+=95"
		}, {
			easing: "linear",
			step: function(now, tween) {
				$("#"+player+"").css({
					transform: "rotate("+(150-tween.pos*150)+"deg)"
				})
			}
		})
		$("#"+player+"").animate({
			left: "-=480"
		}, {
			easing: "easeOutQuad",
			duration: 300
		})
	}
}