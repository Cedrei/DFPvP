"use strict"





let useSkill;


$(function () {
    //var socket = io();

    let playerClass = {}
	function initClass(armor, player) {
		$.get(armor+".js", function(data) {
			eval(player+data);
			playerClass.init()
		})
	}

	useSkill = function(number, character) {
		eval("playerClass.skill"+number+"("+character+")")
	}

    function updatePlayerCharacter() {
		let armor = $('input[name=a-class]:checked', '#formA').val()
		let weapon = $('input[name=a-weapon]:checked', '#formA').val()
		let cape = $('input[name=a-cape]:checked', '#formA').val()
		let helmet = $('input[name=a-helmet]:checked', '#formA').val()
		//socket.emit('updateCharacter', {"armor": armor, "weapon": weapon, "cape": cape, "helmet": helmet});
		$("#player-character>.gear").removeClass("active")

		$("#player-character").removeClass()
		$("#player-character").addClass(armor)
		$("#player-character>.player").attr("src", `img/${armor}.png`)
		initClass(armor, "player")

		$(`#player-character>.${weapon}`).addClass("active")
		$(`#player-character>.${cape}`).addClass("active")
		$(`#player-character>.${helmet}`).addClass("active")

		if (armor == "rogue" && weapon == "dagger") {
			$(`#player-character>.dagger2`).addClass("active")
		} else {
			$(`#player-character>.dagger2`).removeClass("active")
		}
	}

	function updateEnemyCharacter(armor, weapon, cape, helmet) {
		$("#enemy-character>.gear").removeClass("active")

		$("#enemy-character").removeClass()
		$("#enemy-character").addClass(armor)
		$("#enemy-character>.player").attr("src", `img/${armor}.png`)

		$(`#enemy-character>.${weapon}`).addClass("active")
		$(`#enemy-character>.${cape}`).addClass("active")
		$(`#enemy-character>.${helmet}`).addClass("active")

		if (armor == "rogue" && weapon == "dagger") {
			$(`#enemy-character>.dagger2`).addClass("active")
		} else {
			$(`#enemy-character>.dagger2`).removeClass("active")
		}
	}

    $("#formA").change(updatePlayerCharacter)

	updatePlayerCharacter()
	updateEnemyCharacter("warrior", "sword", "cape", "none")

	/*socket.on('updateCharacter', function(msg){
      updateEnemyCharacter(msg.armor, msg.weapon, msg.cape, msg.helmet)
    });*/

 });