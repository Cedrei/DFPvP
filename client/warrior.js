Class = {
	init: function () {
		$("#skill1").css("background-image", 'url("img/icon/lightning.png")')
		$("#skill2").css("background-image", 'url("img/icon/fear.png")')
		$("#skill3").css("background-image", 'url("img/icon/chicken.png")')
		$("#skill4").css("background-image", 'url("img/icon/knife.png")')
		$("#skill5").css("background-image", 'url("img/icon/stun.png")')
		$("#skill6").css("background-image", 'url("img/icon/shield.png")')
		$("#skill7").css("background-image", 'url("img/icon/power.png")')
		$("#skill8").css("background-image", 'url("img/icon/double.png")')
		$("#skill9").css("background-image", 'url("img/icon/triple.png")')
		$("#skill10").css("background-image", 'url("img/icon/manastrike.png")')
		$("#skill11").css("background-image", 'url("img/icon/multi.png")')
		$("#skill12").css("background-image", 'url("img/icon/light.png")')
		$("#skill13").css("background-image", 'url("img/icon/water.png")')
		$("#skill14").css("background-image", 'url("img/icon/skull.png")')
	},
	skillHP: function(character) {
		$("#"+character+">.projectile").attr("src", "img/icon/HPpot.png")
		$("#"+character+">.projectile").css("height", "45px")
		$("#"+character+">.projectile").css("left", "45px")
		$("#"+character+">.projectile").css("top", "45px")

		$("#"+character+">.projectile").animate({
			left: "-=15",
			top: "-=45"
		}, {
			easing: "linear",
			duration: 600,
			step: function(now, tween) {
				$("#"+character+">.projectile").css({
					transform: "rotate("+(360-tween.pos*90)+"deg)"
				})
			},
			done: function() {
				setTimeout(function() {
					$("#"+character+">.projectile").attr("src", "")
					$("#"+character+">.projectile").css("left", "45px")
					$("#"+character+">.projectile").css("top", "45px")
				}, 300)
			}
		})
	},
	skillMP: function(character) {
		$("#"+character+">.projectile").attr("src", "img/icon/MPpot.png")
		$("#"+character+">.projectile").css("height", "45px")
		$("#"+character+">.projectile").css("left", "45px")
		$("#"+character+">.projectile").css("top", "45px")

		$("#"+character+">.projectile").animate({
			left: "-=15",
			top: "-=45"
		}, {
			easing: "linear",
			duration: 600,
			step: function(now, tween) {
				$("#"+character+">.projectile").css({
					transform: "rotate("+(360-tween.pos*90)+"deg)"
				})
			},
			done: function() {
				setTimeout(function() {
					$("#"+character+">.projectile").attr("src", "")
					$("#"+character+">.projectile").css("left", "45px")
					$("#"+character+">.projectile").css("top", "45px")
				}, 300)
			}
		})
	},
	skill1: function(character) {
		$("#"+character+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+character+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill2: function(character) {
		$("#"+character+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+character+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill3: function(character) {
		$("#"+character+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(360-tween.pos*90)+"deg)"
				})
			}
		})
		$("#"+character+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*90+270)+"deg)"
				})
			}
		})
	},
	skill4: function(character) {
		$("#"+character+">.projectile").attr("src", "img/icon/knife.png")
		$("#"+character+">.projectile").css("height", "45px")
		$("#"+character+">.projectile").css("left", "45px")
		$("#"+character+">.projectile").css("top", "45px")

		$("#"+character+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			step: function(now, tween) {
				$("#"+character+">.projectile").css({
					transform: "rotate("+(tween.pos*1000%360)+"deg)"
				})
			},
			done: function() {
				$("#"+character+">.projectile").attr("src", "")
				$("#"+character+">.projectile").css("left", "45px")
				$("#"+character+">.projectile").css("top", "45px")
			}
		})

	},
	skill5: function(character) {
		$("#"+character+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+character+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill6: function(character) {
		$("#"+character+">.speech-bubble").addClass("active")
		$("#"+character+">.speech-bubble>.speech-text").text("I am ready... bring it on!")
		setTimeout(function() {
		    $("#"+character+">.speech-bubble").removeClass('active');
		}, 3000);
		
	},
	skill7: function(character) {
		let messages = [
			"Battle On!",
			"Cry havoc, and let slip the togs of war!",
			"By the power of #CCCCCC Skull",
			"Kowabunga!",
			"Leeeeroy Jeeeenkins",
			"RAGEEEEEEE",
			"Freeeeeeedom!",
			"Aaaaahhhhh!",
			"Kooooooooraaaa!!",
			"spoooon",
			"Kiiiiiiiiiiiiiiiyaaaaaaaaaaaaaa!!!!!!!!!!!!",
			"Rooooooooaaaaar!"
		]
		$("#"+character+">.speech-bubble").addClass("active")
		$("#"+character+">.speech-bubble>.speech-text").text(messages[Math.floor(Math.random()*messages.length)])
		setTimeout(function() {
		    $("#"+character+">.speech-bubble").removeClass('active');
		    $("#"+character+"").animate({
				left: "+=400",
				top: "-=95"
			}, {
				easing: "easeInQuad",
				step: function(now, tween) {
					$("#"+character+"").css({
						transform: "rotate("+tween.pos*90+"deg)"
					})
				}
			})
			$("#"+character+"").animate({
				left: "-=400",
				top: "+=95"
			}, {
				easing: "easeOutQuad",
				duration: 800,
				step: function(now, tween) {
					$("#"+character+"").css({
						transform: "rotate("+(tween.pos*270+90)+"deg)"
					})
				}
			})
		}, 3000);
	},
	skill0: function(character) {
		$("#"+character+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+character+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill8: function(character) {
		$("#"+character+"").animate({
			left: "+=400",
			top: "-=35"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		}).delay(250).animate({
			top: "-=60"
		}, {
			easing: "linear",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*360+90)+"deg)"
				})
			}
		})
		$("#"+character+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill9: function(character) {
		$("#"+character+"").animate({
			left: "+=360",
			top: "-=20"
		}, {
			duration: 360,
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+tween.pos*20+"deg)"
				})
			}
		})
		$("#"+character+"").animate({
			left: "+=40"
		}, {
			duration: 300,
			easing: "linear",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(20+tween.pos*160)+"deg)"
				})
			}
		}).delay(350).animate({
			top: "-=20"
		}, {
			duration: 300,
			easing: "linear",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(180-tween.pos*180)+"deg)"
				})
			}
		}).delay(350).animate({
			top: "-=20"
		}, {
			duration: 300,
			easing: "linear",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*180)+"deg)"
				})
			}
		}).animate({
			left: "-=400",
			top: "+=60"
		}, {
			easing: "easeOutQuad",
			duration: 600,
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*180+180)+"deg)"
				})
			}
		})
	},
	skill10: function(character) {
		$("#"+character+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+character+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill11: function(character) {
		$("#"+character+">.projectile").attr("src", "img/icon/knife.png")
		$("#"+character+">.projectile").css("height", "45px")
		$("#"+character+">.projectile").css("left", "45px")
		$("#"+character+">.projectile").css("top", "45px")

		$("#"+character+">.projectile").animate({
			left: "+=430"
		}, {
			easing: "linear",
			duration: 1000,
			step: function(now, tween) {
				$("#"+character+">.projectile").css({
					transform: "rotate("+(tween.pos*1000%360)+"deg)"
				})
			},
			done: function() {
				$("#"+character+">.projectile").attr("src", "")
				$("#"+character+">.projectile").css("left", "45px")
				$("#"+character+">.projectile").css("top", "45px")
			}
		})
	},
	skill12: function(character) {
		$("#"+character+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+character+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill13: function(character) {
		$("#"+character+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+character+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	},
	skill14: function(character) {
		$("#"+character+"").animate({
			left: "+=400",
			top: "-=95"
		}, {
			easing: "easeInQuad",
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+tween.pos*90+"deg)"
				})
			}
		})
		$("#"+character+"").animate({
			left: "-=400",
			top: "+=95"
		}, {
			easing: "easeOutQuad",
			duration: 800,
			step: function(now, tween) {
				$("#"+character+"").css({
					transform: "rotate("+(tween.pos*270+90)+"deg)"
				})
			}
		})
	}
}