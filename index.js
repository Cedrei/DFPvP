"use strict"

const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.get('*', function(req, res){
  res.sendFile(__dirname + '/client' + req.path);
});

io.on('connection', function(socket){
  socket.broadcast.emit('hi');
  socket.on('updateCharacter', function(msg){
  	console.log(msg)
    socket.broadcast.emit('updateCharacter', msg);
  });
});
    

http.listen(3000, function(){
  console.log('listening on *:3000');
});